import React,{Component} from "react";
import { StyleSheet, Text, View, SafeAreaView,TextInput } from "react-native";
import AgeValidator from "./components/ageValidator/AgeValidator";
import MyList from "./components/myList/MyList";

export default class App extends Component {
 /* constructor(props){
    super(props);
    this.state ={
      textValue:'',
      count:0,
    }
  }
  changeTextInput = text => {
    console.log(text)
    this.setState({textValue: text})
  };

  onPress = () =>{
    this.setState({count: this.state.count + 1,
    });
  };
  */

  render(){
    return(
      <View style={styles.container}>
        <View style={styles.text}>
          <Text>Ingrese su edad</Text>
        </View>
        <AgeValidator/>
        <MyList/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingHorizontal:10,
    backgroundColor:'#FFE001'
  },

  text: {
    alignItems: 'center',
    padding: 10,
  },

  
});