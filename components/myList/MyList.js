import React, {Component} from "react";
import { View, FlatList, StyleSheet, Text, TouchableOpacity, Image} from "react-native"

export default class App extends Component{
    constructor(props){
        super(props)
        this.state ={
            lista:[
                {
                    nombre:'Zhongli',
                    img:'https://static.wikia.nocookie.net/gen-impact/images/7/7b/Zhongli_Card.png/revision/latest/scale-to-width-down/2000?cb=20210303044606&path-prefix=es'
                },

                {
                    nombre:'Albedo',
                    img:'https://media.vandal.net/m/3-2021/20213111252326_1.jpg'
                },
                {
                    nombre:'Bennet',
                    img:'https://media.vandal.net/m/9-2020/2020930106035_5.jpg.webp'
                },
                {
                    nombre:'Xiao',
                    img:'https://static.wikia.nocookie.net/gen-impact/images/7/7f/Xiao_Card.png/revision/latest/smart/width/250/height/250?cb=20210303043521&path-prefix=es'
                },
                {
                    nombre:'Razor',
                    img:'https://moongaming.es/img/cms/Blogs/ghensin/2020930108357_12.jpg'
                },
            ]
        }
    }

    renderItem = ({item}) =>(
        <TouchableOpacity>
            <View style={styles.itemContainer}>
                <Image 
                    style={{width:100, height:100, borderBottomRightRadius:60}}
                    source={{uri: item.img}}
                />
                    <Text style={styles.itemName}>{item.nombre}</Text>
            </View>
        </TouchableOpacity>
    )
    keyExtractor = (item,index) => index.toString()

    FlatListSeparador = () => {
        return(
            <View style={{height:1, width:'100%', backgroundColor:'black'}}/>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.lista}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.FlatListSeparador}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop: 20,
    },

    itemContainer:{
        flex:1,
        flexDirection:'row',
        marginLeft:20,
        justifyContent:'flex-start',
        margin: 15,
    },
    itemName:{
        marginLeft:20,
        fontSize:20,
    },

});


